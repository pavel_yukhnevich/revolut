import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.ContentTypes._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import model.dto._

import scala.util.Random
import org.scalatest._
import slick.jdbc.H2Profile.api._

import scala.concurrent.Await
import scala.concurrent.duration._

class ServerSpec extends FlatSpec with Matchers with BeforeAndAfter with ScalatestRouteTest with MoneyTransfer {
  import spray.json.DefaultJsonProtocol._
  override def testConfigSource =
    """
      |akka.loglevel = "WARNING"
      |h2mem.url = "jdbc:h2:mem:test"
      |h2mem.driver = org.h2.Driver
      |h2mem.connectionPool = disabled
      |h2mem.keepAliveConnection = true
    """.stripMargin
  override val config = testConfig
  val superAccount = AccountDto(1, "super", 0)

  override def beforeAll() = {
    super.beforeAll()
    val testUsers = DBIO.seq(
      accounts += (2, Random.nextString(10), 0),
      accounts += (3, Random.nextString(10), 0),
      accounts += (4, Random.nextString(10), 99.99999)
    )

    Await.result(preStart, 20 seconds)
    Await.result(db.run(testUsers), 10 seconds)
  }

  "Service" should "respond to account/1 request with super user" in {
    Get(s"/account/1") ~> routes ~> check {
      status shouldBe OK
      contentType shouldBe `application/json`
      responseAs[AccountDto] shouldBe superAccount
    }
  }

  it should "create account" in {
    Post(s"/account", AccountInDto(Random.nextString(10))) ~> routes ~> check {
      status shouldBe StatusCodes.OK
      contentType shouldBe `application/json`
      responseAs[IdDto].id > 0
   }
  }

  it should "respond not create negative balances for usual users" in {
    Post(s"/transfer", MoneyTransferDto(2, 3, 1.55)) ~> routes ~> check {
      status shouldBe StatusCodes.InternalServerError
    }
    Post(s"/transfer", MoneyTransferDto(2, 3, -1.55)) ~> routes ~> check {
      status shouldBe StatusCodes.InternalServerError
    }
  }

  it should "proceed valid transfer operations" in {
    Post(s"/transfer", MoneyTransferDto(1, 2, 10.55)) ~> routes ~> check {
      status shouldBe StatusCodes.OK
    }
    Post(s"/transfer", MoneyTransferDto(2, 3, 10.54)) ~> routes ~> check {
      status shouldBe StatusCodes.OK
    }

    Get(s"/accounts") ~> routes ~> check {
      status shouldBe StatusCodes.OK
      contentType shouldBe `application/json`
      val response = responseAs[Seq[AccountDto]]
      response.find(_.id == 2).getOrElse(fail("account is not in list")).balance shouldEqual 0.01
      response.find(_.id == 3).getOrElse(fail("account is not in list")).balance shouldEqual 10.54
    }

    Get(s"/transactions") ~> routes ~> check {
      status shouldBe StatusCodes.OK
      contentType shouldBe `application/json`
      responseAs[Seq[TransactionDto]].length shouldEqual 2
    }
  }
}