package repository

import model.dto.{AccountDto, TransactionDto}
import model.entity.{Accounts, Transactions}
import slick.jdbc.H2Profile.api._
import slick.lifted.TableQuery

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
class BalanceService(db: Database) {
  val accounts = TableQuery[Accounts]
  val transactions = TableQuery[Transactions]
  def find(id: Long): Future[Option[AccountDto]] = {
    db.run(accounts.filter(_.id === id).result.headOption).map{
      case Some((id, username, balance)) => Some(AccountDto(id, username, balance))
      case _ => Option.empty[AccountDto]
    }
  }

  def listAll: Future[Seq[AccountDto]] = {
    db.run(accounts.result.map(_.map{case (id, username, balance) => AccountDto(id, username, balance)}))
  }

  def listAllTransactions: Future[Seq[TransactionDto]] = {
    db.run(transactions.result.map(_.map{case (id, from, to, amount, created) =>
      TransactionDto(id, from, to, amount, created.getTime)}))
  }

  def create(username: String): Future[Long] = {
    val insertAction = (accounts returning accounts.map(_.id)) += (0L, username, 0L)
    db.run(insertAction)
  }

  def transfer(from: Long, to: Long, amount: BigDecimal): Future[Unit] = {
    val fromAccount = accounts.filter(_.id === from)
    val toAccountBalance = accounts.filter(_.id === to).map(_.balance)

    val query =
      DBIO.seq(
        for {from <- fromAccount.forUpdate.result.headOption
             _ <- from match {
               case Some((id, username, balance)) if (balance < amount && username != "super") => throw new Exception("Low balance")
               case Some((id, username, balance)) => fromAccount.map(_.balance).update(balance - amount)
               case _ => throw new Exception("Account not found")}
             } yield ()
        , for {
          toBalance <- toAccountBalance.forUpdate.result.headOption
          _ <- toBalance match {
            case Some(balance) => toAccountBalance.update(balance + amount)
            case _ => throw new Exception("Account not found")
          }
        } yield (),
        transactions.map(t => (t.id, t.from, t.to, t.amount)) += (0L, from, to, amount))


    db.run(query.transactionally)
  }
}
