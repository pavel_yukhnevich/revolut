package model.entity

import slick.jdbc.H2Profile.api._

class Accounts(tag: Tag) extends Table[(Long, String, BigDecimal)](tag, "accounts") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def username = column[String]("username", O.Unique)
  def balance = column[BigDecimal]("balance", O.Default(0))
  def * = (id, username, balance)
}