package model.entity

import java.sql.Timestamp

import slick.jdbc.H2Profile.api._
import slick.lifted.TableQuery

class Transactions(tag: Tag) extends Table[(Long, Long, Long, BigDecimal, Timestamp)](tag, "transactions") {
  val accounts = TableQuery[Accounts]

  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def from = column[Long]("from")
  def to = column[Long]("to")
  def amount = column[BigDecimal]("amount")
  def fromAccount = foreignKey("from_fk", from, accounts)(_.id)
  def toAccount = foreignKey("to_fk", to, accounts)(_.id)
  def created = column[Timestamp]("created", O.SqlType("timestamp default now()"))

  def * = (id, from, to, amount, created)
}