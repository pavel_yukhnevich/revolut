package model.dto

case class AccountDto(id: Long, username: String, balance: BigDecimal)