package model.dto


case class TransactionDto(id: Long, from: Long, to: Long, amount: BigDecimal, created: Long)
