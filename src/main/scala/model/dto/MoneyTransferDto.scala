package model.dto

case class MoneyTransferDto(from: Long, to: Long, amount: BigDecimal)