import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory
import model.dto._
import model.entity.{Accounts, Transactions}
import repository.BalanceService
import slick.jdbc.H2Profile.api._
import slick.lifted.TableQuery
import spray.json.DefaultJsonProtocol._

import scala.concurrent.ExecutionContextExecutor
import scala.io.StdIn
import scala.util.{Failure, Success}


trait MoneyTransfer {
  implicit val accountFormat = jsonFormat3(AccountDto)
  implicit val transactionFormat = jsonFormat5(TransactionDto)
  implicit val idFormat = jsonFormat1(IdDto)
  implicit val accountInFormat = jsonFormat1(AccountInDto)
  implicit val moneyTransferFormat = jsonFormat3(MoneyTransferDto)
  val config = ConfigFactory.load()
  val db = Database.forConfig("h2mem")
  val service = new BalanceService(db)
  val transactions = TableQuery[Transactions]
  val accounts = TableQuery[Accounts]

  def preStart = {
    val setup = DBIO.seq(
      (accounts.schema ++ transactions.schema).create,
      accounts += (1, "super", 0)
    )
    db.run(setup)
  }

  implicit val system: ActorSystem
  implicit val materializer: ActorMaterializer
  implicit def executor: ExecutionContextExecutor

  val routes: Route =
    get {
      pathPrefix("account" / LongNumber) { id =>
        val maybeItem = service.find(id)
        onComplete(maybeItem) {
          case Success(Some(item)) => complete(item)
          case Success(None) => complete(StatusCodes.NotFound)
          case Failure(f) => complete(StatusCodes.InternalServerError -> f.getMessage)
        }
      }
    } ~
    get {
      pathPrefix("accounts") {
        val list = service.listAll
        onComplete(list) {
          case Success(_) => complete(list)
          case Failure(f) => complete(StatusCodes.InternalServerError -> f.getMessage)
        }
      }
    } ~
    get {
      pathPrefix("transactions") {
        val list = service.listAllTransactions
        onComplete(list) {
          case Success(_) => complete(list)
          case Failure(f) => complete(StatusCodes.InternalServerError -> f.getMessage)
        }
      }
    } ~
    post {
      path("account") {
        entity(as[AccountInDto]) { account =>
          val result = service.create(account.username)
          onComplete(result) {
            case Success(id) =>  complete(IdDto(id))
            case Failure(f) => complete(StatusCodes.InternalServerError -> f.getMessage)
          }
        }
      }
    } ~
    post {
      path("transfer") {
        entity(as[MoneyTransferDto]) { moneyTransferDto =>
          if (moneyTransferDto.amount <= 0) {
            throw new Exception(s"Invalid amount value: ${moneyTransferDto.amount}")
          }
          val result = service.transfer(moneyTransferDto.from, moneyTransferDto.to, moneyTransferDto.amount)
          onComplete(result) {
            case Success(_) =>  complete(StatusCodes.OK)
            case Failure(f) => complete(StatusCodes.InternalServerError -> f.getMessage)
          }
        }
      }
    }
}

object WebServer extends App with MoneyTransfer {
  override implicit val system = ActorSystem()
  override implicit val executor = system.dispatcher
  override implicit val materializer = ActorMaterializer()

  override val config = ConfigFactory.load()
  preStart.foreach { _ =>
    val server = config.getString("server.host")
    val port = config.getInt("server.port")
    val bindingFuture = Http().bindAndHandle(routes, server, port)

    println(s"Server online at http://$server:$port/\nPress RETURN to stop...")
    StdIn.readLine()
    bindingFuture
      .flatMap(_.unbind())
      .onComplete(_ ⇒ system.terminate())
  }
}