name := "MoneyTransfer"

version := "1.0"

scalaVersion := "2.12.1"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-http" % "10.0.5",
  "com.typesafe.akka" %% "akka-http-spray-json" % "10.0.5",
  "com.typesafe.akka" %% "akka-http-testkit" % "10.0.0",
  "com.typesafe.slick" % "slick_2.12" % "3.2.0",
  "org.slf4j" % "slf4j-nop" % "1.7.25",
  "com.h2database" % "h2" % "1.4.194",
  "org.scalatest" %% "scalatest" % "3.0.1" % "test"
)